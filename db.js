const { LocalStorage } = require("node-localstorage");
const uuid = require("uuid/v4");
const fs = require("fs");
const path = require("path");
const os = require("os");
const Logger = require("./logger");

module.exports = class Database {
    constructor(path) {
        this._defaultPath = path;
        this._database = new LocalStorage(path, Number.MAX_SAFE_INTEGER);

        if (!this.gamemodes) {
            this.gamemodes = {};
        }

        if (!this.players) {
            this.players = {};
        }

        if (!this.config) {
            this.config = {
                users: {},
                port: 13579,
                killOnPull: false
            };
        }
    }

    /**
     * @returns {object} Database contents.
     */
    get data() {
        return this._database;
    }

    get gamemodes() {
        return JSON.parse(this.data.gamemodes);
    }

    set gamemodes(value) {
        this.data.setItem("gamemodes", JSON.stringify(value));
    }

    get players() {
        return JSON.parse(this.data.players);
    }

    set players(value) {
        this.data.setItem("players", JSON.stringify(value));
    }

    get config() {
        return JSON.parse(this.data.config);
    }

    set config(value) {
        this.data.setItem("config", JSON.stringify(value));
    }

    setPlayer(battletag, newPlayer) {
        // Add or replace player
        const temp = this.players;
        temp[battletag] = newPlayer;
        this.players = temp;
    }

    setGamemode(id, newGamemode) {
        // Add or replace mode
        const temp = this.gamemodes;
        temp[id] = newGamemode;
        this.gamemodes = temp;
    }

    exportDatabase() {
        return {
            gamemodes: this.gamemodes,
            players: this.players
        };
    }

    importDatabase(json) {
        if (!json) {
            return;
        }

        this._backup = this.exportDatabase();
        fs.writeFileSync(path.join(os.tmpdir(), "watchover-database-backup-" + uuid() + ".json"), JSON.stringify(this._backup));
        
        if (json.gamemodes) {
            this.gamemodes = json.gamemodes;
        }

        if (json.players) {
            this.players = json.players;
        }
    }

    /**
     * Re-checks the status of gamemodes. If we hit the endDate, deactivate the gamemode and award purple points.
     * Additionally triggers to re-check itself after the next gamemode ends (or a maxint delay of like 25 days
     * if it's longer than that until the next gamemode end).
     */
    checkGamemodeExpiration() {
        const now = new Date().getTime();
        for (const modeId in this.gamemodes) {
            const gamemode = this.gamemodes[modeId];
            if (!gamemode.isActive || gamemode.endDate > now) {
                continue;
            }

            // If any gamemode expired, award purple points
            Logger.info("Gamemode", gamemode.name, "Season", gamemode.season, "has ended!");
            for (const battletag in this.players) {
                let rewardPP = 0;
                const player = this.players[battletag];
                const maxSR = {};

                // Player didn't play this mode yet
                if (!player.matches[modeId]) {
                    continue;
                }

                for (const match of player.matches[modeId]) {
                    if (match.sr === undefined) {
                        continue;
                    }

                    if (!maxSR[match.role] || match.sr > maxSR[match.role]) {
                        maxSR[match.role] = match.sr;
                    }
                }

                function srToRankIndex(sr) {
                    if (sr < 1500) {
                        return 0; // Bronze
                    } else if (sr < 2000) {
                        return 1; // Silver
                    } else if (sr < 2500) {
                        return 2; // Gold
                    } else if (sr < 3000) {
                        return 3; // Platinum
                    } else if (sr < 3500) {
                        return 4; // Diamond
                    } else if (sr < 4000) {
                        return 5; // Master
                    } else {
                        return 6; // Grandmaster
                    }
                }

                for (const role in maxSR) {
                    rewardPP += gamemode.rankPP[srToRankIndex(maxSR[role])];
                    Logger.debug(battletag, "gets", gamemode.rankPP[srToRankIndex(maxSR[role])], "for", role, "with his", maxSR[role], "SR (max)");
                }

                Logger.debug(battletag, "gets", rewardPP, "total");
                if (rewardPP > 0) {
                    // Add his PP
                    player.purplePoints += rewardPP;
                    this.setPlayer(battletag, player);
                }
            }

            // Obviously expired now
            gamemode.isActive = false;
            this.setGamemode(modeId, gamemode);
        }

        // Now that our expiry list is up-to-date, go through a second time and find out which mode will require this check again
        let nextToExpire;
        for (const modeId in this.gamemodes) {
            const gamemode = this.gamemodes[modeId];
            if (gamemode.isActive && gamemode.endDate !== undefined && (nextToExpire === undefined || gamemode.endDate < nextToExpire)) {
                // We must find out which active mode will expire next
                nextToExpire = modeId;
            }
        }

        clearTimeout(this._nextExpiringMode);
        if (nextToExpire) {
            // Give it some leeway
            const leeway = 1000;
            const expireMode = this.gamemodes[nextToExpire];
            const expirationDate = expireMode.endDate;
            const delay = expirationDate - now + leeway;
            let timeoutDelay = delay;
            if (timeoutDelay > 2147483647) {
                // setTimeout does not allow too huge values, so clamp and just recheck instead
                timeoutDelay = 2147483647;
            }
            this._nextExpiringMode = setTimeout(() => this.checkGamemodeExpiration(), timeoutDelay);
            
            let endsIn = Math.round(delay / 1000 / 60 / 60 / 24 * 10) / 10;
            if (endsIn > 1) {
                endsIn = Math.round(endsIn);
            }
            Logger.info("Next gamemode", expireMode.name, "Season", expireMode.season, "will end in", endsIn, "days.");
        } else {
            Logger.info("No competitive seasons are active anymore.");
        }
    }
};

// Archives
module.dontports = class DatabaseBetterAndBackwardsCompatibleAndOptimized {
    constructor(path) {
        this._defaultPath = path;
        this._database = new LocalStorage(path, Number.MAX_SAFE_INTEGER);

        this.players = JSON.parse(this._database.players);
        this.gamemodes = JSON.parse(this._database.gamemodes);

        setInterval(() => {
            // <quote> if(JSON.stringify) </quote>
            if (JSON.stringify) { /* */ }
            this._database.players = JSON.stringify(this.players);
            this._database.gamemodes = JSON.stringify(this.gamemodes);
        }, 100000 /* optimize value later */);
    }
};
