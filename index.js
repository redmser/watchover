const uuid = require("uuid/v4");
const path = require("path");
const Logger = require("./logger");
const db = new (require("./db"))(path.join(__dirname, "database"));
db.checkGamemodeExpiration();

const express = require("express");
const bodyparser = require("body-parser");
const cookieParser = require("cookie-parser");
const basicAuth = require("express-basic-auth");
const app = express();
 
const { exec } = require("child_process");

app.set("view engine", "ejs");
app.use("/public", express.static(path.join(__dirname, "public/")));
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(cookieParser());
app.use(basicAuth({
    users: db.config.users,
    challenge: true,
    realm: "watchover"
}));

/**
 * Generate render args.
 * @param {object} options Object options.
 * @returns {object} Populated object options.
 */
function generateRenderArgs(options) {
    options.title = options.title || null;
    if (options.gamemodes === "basic") {
        options.gamemodes = db.gamemodes;
    } else if (options.gamemodes === "placements") {
        // Replace detailed gamemode stats with basic placement match statistics
        if (!options.player) {
            throw new Error("Can't get placement info of gamemodes without a player.");
        }

        options.gamemodes = {};
        for (const modeId in db.gamemodes) {
            const mode = db.gamemodes[modeId];
            const matches = options.player.matches[modeId] || [];
            if (mode.isRoleLocked) {
                mode.placements = 5;
                mode.matches = JSON.stringify({
                    tank: matches.filter(m => m.role === "tank").length,
                    dps: matches.filter(m => m.role === "dps").length,
                    support: matches.filter(m => m.role === "support").length
                });
            } else {
                mode.placements = 10;
                mode.matches = JSON.stringify({
                    any: matches.length
                });
            }

            options.gamemodes[modeId] = mode;
        }
    } else if (options.gamemodes === "max") {
        // Only get the max season's info for each game mode (compared by name)
        const uniqueModes = {};
        for (const modeId in db.gamemodes) {
            const mode = db.gamemodes[modeId];
            mode._id = modeId;
            if (!uniqueModes[mode.name] || uniqueModes[mode.name].season < mode.season) {
                uniqueModes[mode.name] = mode;
            }
        }

        // Remap mode id back to an object
        options.gamemodes = {};
        for (const mode of Object.values(uniqueModes)) {
            const modeId = mode._id;
            delete mode._id;
            options.gamemodes[modeId] = mode;
        }
    }

    if (options.player === "all") {
        options.player = db.players;
    }

    return options;
}

app.get("/", (req, res) => {
    res.render("index.ejs", generateRenderArgs({}));
});

app.get("/settings", async (req, res) => {
    let player;

    const battletag = req.cookies.battletag;
    if (battletag) {
        player = db.players[battletag];
    }

    if (player) {
        res.render("settings.ejs", generateRenderArgs({ title: "User settings", purplePoints: player.purplePoints }));
    } else {
        res.redirect("/login");
    }
});

app.post("/settings", (req, res) => {
    const battletag = req.cookies.battletag;
    if (!battletag) {
        res.status(401).send();
        return;
    }

    const player = db.players[battletag];
    player.purplePoints = Math.max(0, Math.min(6000, parseInt(req.body.purple)));
    db.setPlayer(battletag, player);
    res.redirect("/settings");
});

app.get("/stats/add", (req, res) => {
    const battletag = req.cookies.battletag;
    if (!battletag) {
        res.redirect("/login");
        return;
    }

    const player = db.players[battletag];
    if (!player) {
        res.status(401).send();
        return;
    }

    res.render("add-stats.ejs", generateRenderArgs({ title: "Add stats", player: player, gamemodes: "placements" }));
});
app.post("/stats/add", (req, res) => {
    const battletag = req.cookies.battletag;
    if (!battletag) {
        res.redirect("/login");
        return;
    }

    const player = db.players[battletag];
    if (!player) {
        res.status(401).send();
        return;
    }

    const modeId = req.body.gamemode;
    const mode = db.gamemodes[modeId];
    if (!player.matches[modeId]) {
        player.matches[modeId] = [];
    }

    // Was this a placement match?
    let placementCount;
    let placementRequirement;
    if (mode.isRoleLocked) {
        const sameRoleMatches = player.matches[modeId].filter(m => m.role === req.body.role).length;
        placementCount = sameRoleMatches;
        placementRequirement = 5;
    } else {
        const matchCount = player.matches[modeId].length;
        placementCount = matchCount;
        placementRequirement = 10;
    }

    const match = {
        role: mode.isRoleLocked ? req.body.role : "any",
        date: new Date().getTime()
    };

    // The -1 is required because 5/5th (or 10/10)th placement already gives you the SR
    if (placementCount < placementRequirement - 1) {
        // For usual placements, we only know the win/loss/draw result
        match.result = req.body.result;
    } else if (placementCount === placementRequirement - 1) {
        // The last placement match has both a result and the SR we placed in
        match.result = req.body.result;
        match.sr = parseInt(req.body.sr);
    } else {
        // For usual matches, only record the SR value
        match.sr = parseInt(req.body.sr);
    }

    // Give player the correct amount of purple points
    const roleMatches = player.matches[modeId].filter(m => m.role === match.role);
    const lastMatch = roleMatches[roleMatches.length - 1];
    let lastSR;
    if (lastMatch) {
        lastSR = lastMatch.sr;
    }
    if (match.result === "win" || (match.sr !== undefined && lastSR !== undefined && match.sr > lastSR)) {
        player.purplePoints += mode.winPP;
    } else if (match.result === "draw" || (match.sr !== undefined && lastSR !== undefined && match.sr === lastSR)) {
        player.purplePoints += mode.drawPP;
    }

    player.matches[modeId].push(match);
    db.setPlayer(battletag, player);

    // We need more comp!!! Show same page again
    res.redirect("/stats/add");
});

app.get("/gamemode/add", (req, res) => {
    res.render("add-gamemode.ejs", generateRenderArgs({ title: "Create gamemode", gamemodes: "max" }));
});
app.post("/gamemode/add", (req, res) => {
    const gamemode = {
        name: req.body.gamemode === "_new" ? req.body.name : req.body.gamemode,
        season: parseInt(req.body.season),
        isRoleLocked: req.body.rolelocked === "on",
        isActive: true,
        rankPP: [parseInt(req.body.pp1), parseInt(req.body.pp2), parseInt(req.body.pp3), parseInt(req.body.pp4), parseInt(req.body.pp5), parseInt(req.body.pp6), parseInt(req.body.pp7)],
        winPP: parseInt(req.body.pp8),
        drawPP: parseInt(req.body.pp9),
        endDate: new Date().getTime() + 1000 * 60 * 60 * 24 * parseInt(req.body["ends-days"])
    };

    db.setGamemode(uuid(), gamemode);

    // This might be the next-to-expire gamemode, so do a check here
    db.checkGamemodeExpiration();
    
    // We probably want to add stats for this now
    res.redirect("/stats/add");
});

app.get("/stats/:battletag", async (req, res) => {
    const battletag = req.params.battletag;
    const player = db.players[battletag];
    if (player) {
        const name = battletag.substr(0, battletag.lastIndexOf("#"));
        player.name = name;
        player.battletag = battletag;

        res.render("stats-user.ejs", generateRenderArgs({ title: `Stats for ${name}`, player: player, gamemodes: "basic" }));
    } else {
        res.status(404).send();
    }
});

app.get("/login", (req, res) => {
    const battletag = req.cookies.battletag;
    if (battletag) {
        // Already logged in
        res.redirect("/stats/" + encodeURIComponent(battletag));
    } else {
        res.render("login.ejs", generateRenderArgs({ title: "Login" }));
    }
});
app.get("/logout", (req, res) => {
    res.render("logout.ejs", generateRenderArgs({ title: "Logout" }));
});

app.post("/player/add", async (req, res) => {
    const battletag = req.body.battletag;
    let player = db.players[battletag];
    if (!player) {
        // New player object
        player = {
            purplePoints: 0,
            matches: {}
        };
        db.setPlayer(battletag, player);
    }
    res.redirect("/stats/" + encodeURIComponent(battletag));
});
app.get("/player/list", (req, res) => {
    res.render("user-list.ejs", generateRenderArgs({ title: "Player list", player: "all" }));
});

app.get("/hero-selector", (req, res) => {
    res.render("hero-selector.ejs", generateRenderArgs({ title: "Hero selector" }));
});

app.get("/config", (req, res) => {
    res.render("data.ejs", generateRenderArgs({ title: "Config" }));
});
app.post("/git/:action", (req, res) => {
    const action = req.params.action;
    if (action === "pull") {
        exec("git pull", (err, stdout, stderr) => {
            res.send({ output: stdout, error: (err || "") + (stderr || "")});

            if (db.config.killOnPull) {
                setTimeout(() => {
                    process.exit(0);
                }, 1000);
            }
        });
    } else if (action === "status") {
        exec("git status", (err, stdout, stderr) => {
            res.send({ output: stdout, error: (err || "") + (stderr || "")});
        });
    } else if (action === "checkout") {
        // git checkout londre && rm -rf
        exec("git checkout \"" + req.body.target.replace(/[^a-zA-Z0-9-]/g, "") + "\"", (err, stdout, stderr) => {
            res.send({ output: stdout, error: (err || "") + (stderr || "")});

            if (db.config.killOnPull) {
                setTimeout(() => {
                    process.exit(0);
                }, 1000);
            }
        });
    } else if (action === "log") {
        const number = Math.max(0, Math.min(20, parseInt(req.body.number)));
        exec("git --no-pager log -n \"" + number + "\"", (err, stdout, stderr) => {
            res.send({ output: stdout, error: (err || "") + (stderr || "")});
        });
    }
});
app.post("/database/:action", (req, res) => {
    const action = req.params.action;
    if (action === "export") {
        // Return the database contents
        res.send(db.exportDatabase());
    } else if (action === "import") {
        // Parse the database contents
        db.importDatabase(req.body);
        res.status(200).send();
    } else {
        res.status(400).send();
    }
});

app.listen(db.config.port, () => {
    Logger.info(`Hosting on port ${db.config.port}!`);
});
