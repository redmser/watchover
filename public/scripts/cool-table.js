window.watchover = {};

/* Supported cell values

colorSchemeFunction:
Function({min, max, cell, header, items}): object | string (assumed to be backgroundColor)

computedValueFunction:
Function({cell, row, col, rowIndex, colIndex}): object

== Header cell properties ==
title               {string}                    If defined, it's a header cell (specify only for first col and first row or it's gonna be lel)
cellsStyle          {object}                    CSS style override for a header, affecting the whole row/column
category            {string}                    Category for batch-referencing multiple headers
colorScheme         {colorSchemeFunction}       Automatically set color based on cell values
sortable            {boolean}                   Header can be clicked to sort.
fixed               {boolean}                   Header is fixed, even when sorted.
hidden              {boolean}                   Header and its row/col does not show up and is not included in computedValue (DO NOT SET THIS! use the functions instead)
min                 {number}                    If defined, min value for color scheme. Else, min of row/col is used.
max                 {number}                    If defined, max value for color scheme. Else, max of row/col is used.

== Data cell properties ==
value               {object}                    Value to use for sorting, computedValues and default display
computedValue       {computedValueFunction}     Automatically computes and returns cell value

== Shared properties ==
style               {object}                    CSS style override for this cell
ignoreColorScheme   {boolean}                   If set to true, the cell won't use the color scheme defined in the header
format              {Function(object): string}  Reformats the value object for display.
rowspan             {number}                    Span cell. Ensure next row(s) have {} objects to compensate.
colspan             {number}                    Span cell. Ensure next column(s) have {} objects to compensate.
tooltip             {string}                    Tooltip text.

*/

window.watchover.CoolTable = class CoolTable {
    constructor() {
        this._data = [];
        this._table = document.createElement("table");
    }

    /**
     * @returns {boolean} Table is empty (no data or headers).
     */
    get isEmpty() {
        return this.rowCount === 0 || this.colCount === 0;
    }

    /**
     * @returns {number} Number of rows including the header's.
     */
    get rowCount() {
        return this._data.length;
    }

    /**
     * @returns {number} Number of columns including the header's.
     */
    get colCount() {
        return this.getRow(0, {includesHeader: true}).length;
    }
    
    /**
     * @returns {boolean} Whether rows have a header on the left. Only works on table bigger than 1 by 1.
     */
    get hasRowHeader() {
        return this._hasRowHeader;
    }

    /**
     * @returns {boolean} Whether columns have a header on the top. Only works on table bigger than 1 by 1.
     */
    get hasColHeader() {
        return this._hasColHeader;
    }

    /**
     * Returns if cell is a header.
     * @param {object} cell Cell object.
     * @returns {boolean} True if header, false if data. 
     */
    static isHeader(cell) {
        return cell.title !== undefined;
    }

    /**
     * Get the row specified by rowIndex.
     * @param {number} rowIndex Row index. Zero may refer to the header row, if column headers are enabled.
     * @param {object} parameters Options object.
     * @param {boolean} parameters.includesHeader True if should include headers. If false, the array's length may be one shorter than colCount.
     * @param {boolean} parameters.includesHidden True if should include currently hidden parameters.
     * @returns {Array<object>} Array of cell objects. Does not manipulate original data array.
     */
    getRow(rowIndex, {includesHeader = false, includesHidden = false} = {}) {
        const row = this._data[rowIndex].slice();
        if (!includesHidden && this.hasColHeader) {
            // Check if columns are hidden and splice
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                const colHeader = this.getColHeader(colIndex);
                if (colHeader.hidden) {
                    row.splice(colIndex, 1);
                }
            }
        }
        if (!includesHeader && this.hasRowHeader) {
            // Remove the first element
            row.splice(0, 1);
        }
        return row;
    }

    /**
     * @param {number} rowIndex Row index.
     * @returns {object} Header cell object from specified row.
     */
    getRowHeader(rowIndex) {
        if (!this.hasRowHeader) {
            return undefined;
        }
        return this.getRow(rowIndex, {includesHeader: true, includesHidden: true})[0];
    }

    /**
     * Get the column specified by colIndex.
     * @param {number} colIndex Column index. Zero may refer to the header column, if row headers are enabled.
     * @param {object} parameters Options object.
     * @param {boolean} parameters.includesHeader True if should include headers. If false, the array's length may be one shorter than rowCount.
     * @param {boolean} parameters.includesHidden True if should include currently hidden parameters.
     * @returns {Array<object>} Array of cell objects. Does not manipulate original data array.
     */
    getCol(colIndex, {includesHeader = false, includesHidden = false} = {}) {
        const col = [];
        for (let rowIndex = (!includesHeader && this.hasColHeader) ? 1 : 0; rowIndex < this.rowCount; rowIndex++) {
            if (!includesHidden && this.hasRowHeader) {
                const rowHeader = this.getRowHeader(rowIndex);
                if (rowHeader.hidden) {
                    continue;
                }
            }
            // Always include header in the row, since colIndex is always zero-indexed on headers
            col.push(this.getRow(rowIndex, {includesHeader: true, includesHidden: true})[colIndex]);
        }
        return col;
    }

    /**
     * @param {number} colIndex Col index.
     * @returns {object} Header cell object from specified col.
     */
    getColHeader(colIndex) {
        if (!this.hasColHeader) {
            return undefined;
        }
        return this.getRow(0, {includesHeader: true, includesHidden: true})[colIndex];
    }

    _generateBody() {
        const body = document.createElement("tbody");

        for (let rowIndex = 0; rowIndex < this.rowCount; rowIndex++) {
            const row = this._data[rowIndex];
            const rowElement = document.createElement("tr");

            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                const cell = row[colIndex];
                let cellElement;

                if (cell.title !== undefined) {
                    // Header cell
                    const rowHeader = this.getRowHeader(rowIndex);
                    const colHeader = this.getColHeader(colIndex);
                    if ((rowHeader && rowHeader.hidden) || (colHeader && colHeader.hidden)) {
                        continue;
                    }

                    cellElement = document.createElement("th");
                    cellElement.innerHTML = cell.title;

                    // Sorting
                    if (cell.sortable) {
                        cellElement.classList.add("ct-sortable");
                        if (cell._sortDirection === 1) {
                            cellElement.classList.add("ct-ascending");
                        } else if (cell._sortDirection === -1) {
                            cellElement.classList.add("ct-descending");
                        }
                        cell._sortDirection = undefined;

                        cellElement.addEventListener("click", () => {
                            let direction;

                            if (cellElement.classList.contains("ct-ascending")) {
                                cellElement.classList.add("ct-descending");
                                direction = -1;
                            } else {
                                cellElement.classList.add("ct-ascending");
                                direction = 1;
                            }

                            cell._sortDirection = direction;

                            if (colIndex === 0 && this.hasRowHeader) {
                                this._sortByRow(rowIndex, direction);
                            } else {
                                this._sortByCol(colIndex, direction);
                            }
                            this.replaceBody();
                        });
                    }
                } else if (cell.value !== undefined) {
                    // Data cell
                    const rowHeader = this.getRowHeader(rowIndex);
                    const colHeader = this.getColHeader(colIndex);
                    if ((rowHeader && rowHeader.hidden) || (colHeader && colHeader.hidden)) {
                        continue;
                    }

                    cellElement = document.createElement("td");
                    cellElement.innerHTML = cell.format !== undefined ? cell.format(cell.value) : cell.value;

                    // Custom CSS styling (row -> col -> cell)
                    // Row
                    if (rowHeader) {
                        for (const key in rowHeader.cellsStyle) {
                            cellElement.style[key] = rowHeader.cellsStyle[key];
                        }
                    }
                    // Col
                    if (colHeader) {
                        for (const key in colHeader.cellsStyle) {
                            cellElement.style[key] = colHeader.cellsStyle[key];
                        }
                    }

                    // Color scheme defined in a header
                    let colorSchemeHeader;
                    let colorSchemeItems;
                    if (rowHeader && rowHeader.colorScheme) {
                        colorSchemeHeader = rowHeader;
                        colorSchemeItems = this.getRow(rowIndex);
                    } else if (colHeader && colHeader.colorScheme) {
                        colorSchemeHeader = colHeader;
                        colorSchemeItems = this.getCol(colIndex);
                    }

                    if (colorSchemeHeader !== undefined && !cell.ignoreColorScheme) {
                        // Remove ignored items
                        colorSchemeItems = colorSchemeItems.filter(r => !r.ignoreColorScheme && r.value !== null && r.value !== undefined).map(r => r.value);
                        const max = colorSchemeHeader.max !== undefined ? colorSchemeHeader.max : Math.max(...colorSchemeItems);
                        const min = colorSchemeHeader.min !== undefined ? colorSchemeHeader.min : Math.min(...colorSchemeItems);

                        const colorResult = colorSchemeHeader.colorScheme({ min, max, cell, header: colorSchemeHeader, items: colorSchemeItems });
                        if (typeof colorResult === "string") {
                            // Assume background color CSS string
                            cellElement.style.backgroundColor = colorResult;
                        } else if (typeof colorResult === "object") {
                            // It might be a RGB object (sad!)
                            if (colorResult.r !== undefined && colorResult.g !== undefined && colorResult.b !== undefined) {
                                cellElement.style.backgroundColor = CoolTable.colorToString(colorResult);
                            } else {
                                // Assume a CSS object
                                for (const key in colorResult) {
                                    cellElement.style[key] = colorResult[key];
                                }
                            }
                        }
                    }
                }

                if (cellElement) {
                    // Cell style
                    for (const key in cell.style) {
                        cellElement.style[key] = cell.style[key];
                    }

                    // TODO: If a header has span, then the following placeholder elements are not headers (or if you do title: "" then they'll create an unwanted <th> element)
                    if (cell.rowspan > 0) {
                        let hidden = 0;
                        // Check how many of my neighbor headers are hidden
                        if (this.hasRowHeader) {
                            const headers = this.getCol(0, { includesHeader: true, includesHidden: true });
                            for (let i = rowIndex + 1; i < Math.min(headers.length - 1, rowIndex + cell.rowspan); i++) {
                                if (headers[i].hidden) {
                                    hidden++;
                                }
                            }
                        }
                        cellElement.rowSpan = cell.rowspan - hidden;
                    }
                    if (cell.colspan > 0) {
                        let hidden = 0;
                        // Check how many of my neighbor headers are hidden
                        if (this.hasColHeader) {
                            const headers = this.getRow(0, { includesHeader: true, includesHidden: true });
                            for (let i = colIndex + 1; i < Math.min(headers.length - 1, colIndex + cell.colspan); i++) {
                                if (headers[i].hidden) {
                                    hidden++;
                                }
                            }
                        }
                        cellElement.colSpan = cell.colspan - hidden;
                    }
                    cellElement.title = cell.tooltip || "";

                    rowElement.appendChild(cellElement);
                }
            }

            // Do not include a row that has no th/td in it
            if (rowElement.childElementCount > 0) {
                body.appendChild(rowElement);
            }
        }
        return body;
    }

    _setHidden(cell, hidden) {
        cell.hidden = hidden === undefined ? !cell.hidden : hidden;
    }

    /**
     * Hide the given row or column.
     * @param {object} cell Reference to header cell.
     * @param {boolean|null} hidden Whether it should be hidden. If null or undefined, toggle hidden status.
     */
    setHidden(cell, hidden) {
        this._setHidden(cell, hidden);
        this.onDataChanged();
    }
    
    /**
     * Hides the given row.
     * @param {number} rowIndex Index of row header to hide.
     * @param {boolean|null} hidden Whether it should be hidden. If null or undefined, toggle hidden status.
     */
    setRowHidden(rowIndex, hidden) {
        const header = this.getRowHeader(rowIndex);
        this.setHidden(header, hidden);
    }

    /**
     * Hides the given column.
     * @param {number} colIndex Index of column header to hide.
     * @param {boolean|null} hidden Whether it should be hidden. If null or undefined, toggle hidden status.
     */
    setColHidden(colIndex, hidden) {
        const header = this.getColHeader(colIndex);
        this.setHidden(header, hidden);
    }

    /**
     * Hides all headers that are part of the given category.
     * @param {string} categoryName Name of the category to hide.
     * @param {boolean|null} hidden Whether it should be hidden. If null or undefined, toggle hidden status.
     */
    setCategoryHidden(categoryName, hidden) {
        const headers = this.getHeaders();
        for (const headerCell of headers) {
            if (headerCell.category === categoryName) {
                this._setHidden(headerCell, hidden);
            }
        }
        this.onDataChanged();
    }
    
    /**
     * Returns all headers of the table, first retrieving column headers then row headers.
     * @param {object} props Settings
     * @param {boolean?} props.includesHidden Whether to include hidden headers.
     */
    getHeaders({includesHidden = true} = {}) {
        let headers = [];
        if (this.hasColHeader) {
            headers = headers.concat(this.getRow(0, {includesHeader: true, includesHidden}));
        }
        if (this.hasRowHeader) {
            headers = headers.concat(this.getCol(0, {includesHeader: true, includesHidden}));
        }
        if (this.hasColHeader && this.hasRowHeader) {
            // Don't include the top-left cell twice
            headers.shift();
        }
        return headers;
    }

    static unlerp(value, min, max) {
        return (value - min) / (max - min);
    }

    static getColorGradient(colorScheme, percentage) {
        if (percentage < 0) {
            percentage = 0;
        } else if (percentage > 1) {
            percentage = 1;
        }

        let color;
        if (colorScheme.middle !== undefined) {
            if (percentage < 0.5) {
                percentage *= 2;
                color = {
                    r: Math.floor(colorScheme.start.r * (1 - percentage) + colorScheme.middle.r * percentage),
                    g: Math.floor(colorScheme.start.g * (1 - percentage) + colorScheme.middle.g * percentage),
                    b: Math.floor(colorScheme.start.b * (1 - percentage) + colorScheme.middle.b * percentage)
                };
            } else {
                percentage -= 0.5;
                percentage *= 2;
                color = {
                    r: Math.floor(colorScheme.middle.r * (1 - percentage) + colorScheme.end.r * percentage),
                    g: Math.floor(colorScheme.middle.g * (1 - percentage) + colorScheme.end.g * percentage),
                    b: Math.floor(colorScheme.middle.b * (1 - percentage) + colorScheme.end.b * percentage)
                };
            }
        } else {
            color = {
                r: Math.floor(colorScheme.start.r * (1 - percentage) + colorScheme.end.r * percentage),
                g: Math.floor(colorScheme.start.g * (1 - percentage) + colorScheme.end.g * percentage),
                b: Math.floor(colorScheme.start.b * (1 - percentage) + colorScheme.end.b * percentage)
            };
        }

        return CoolTable.colorToString(color);
    }

    static colorToString(color) {
        if (!color) {
            return color;
        }
        return "rgb(" + [color.r, color.g, color.b].join(",") + ")";
    }
    
    /**
     * Prints a debug display for the table in the console.
     */
    debugDisplay() {
        for (let rowIndex = 0; rowIndex < this.rowCount; rowIndex++) {
            const row = this._data[rowIndex];
            let lineContent = "";
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                const cell = row[colIndex];
                const value = cell.title || String(cell.value);
                lineContent += value.padEnd(20);
            }
            console.log(lineContent);
        }
    }

    static _compareValues(a, b) {
        if (typeof a === "number" && typeof b === "number") {
            return a - b;
        }
        if (typeof a === "string" && typeof b === "string") {
            return a.localeCompare(b);
        }
    }

    _sortByCol(colIndex, direction) {
        // We clicked on a column, shuffle the rows
        this._data.sort((a, b) => {
            // a = [ {}                  , {title: "games played"}       , {title: "wins"}  , {title: "win%"}                             ],
            // b = [ {title: "tank"}    , {value: 3, color: "#432431"}   , {value: 2}        , {value: 0.2, format: (v) => (v * 100) + "%"} ],
            const aCell = a[colIndex];
            const bCell = b[colIndex];

            // Currently sorting row should remain untouched
            if (aCell._sortDirection !== undefined) {
                return 0;
            } else if (bCell._sortDirection !== undefined) {
                return 0;
            }

            // Titles always at the top
            if (aCell.title !== undefined) {
                return -1;
            } else if (bCell.title !== undefined) {
                return 1;
            }

            // Fixed at top/bottom, if defined
            if (a[0].fixed !== undefined) {
                return a[0].fixed === "start" ? -1 : 1;
            } else if (b[0].fixed !== undefined) {
                return b[0].fixed === "start" ? 1 : -1;
            }

            // Compare the values
            return direction * CoolTable._compareValues(aCell.value, bCell.value);
        });
    }

    _sortByRow(rowIndex, direction) {
        // We clicked on a row, shuffle the columns

        // sortRow = [ {title: "tank"}    , {value: 3, color: "#432431"}   , {value: 2}        , {value: 0.2, format: (v) => (v * 100) + "%"} ],
        const sortRow = this._data[rowIndex]; 
        const indices = [...Array(sortRow.length).keys()];

        indices.sort((a, b) => {
            const aCell = sortRow[a]; // {title: "tank"}
            const bCell = sortRow[b]; // {value: 3, color: "#432431"}

            // Currently sorting column should remain untouched
            if (aCell._sortDirection !== undefined) {
                return 0;
            } else if (bCell._sortDirection !== undefined) {
                return 0;
            }

            // Titles always at the left
            if (aCell.title !== undefined) {
                return -1;
            } else if (bCell.title !== undefined) {
                return 1;
            }

            // Fixed at left/right, if defined
            if (this._data[0][a].fixed !== undefined) {
                return this._data[0][a].fixed === "start" ? -1 : 1;
            } else if (this._data[0][b].fixed !== undefined) {
                return this._data[0][b].fixed === "start" ? 1 : -1;
            }

            // Compare the values
            return direction * (aCell.value - bCell.value);
        });

        // Apply the index swaps to all rows
        for (let rowIndex = 0; rowIndex < this.rowCount; rowIndex++) {
            const row = this._data[rowIndex];
            const sortedRow = new Array(row.length);
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                // row [{1}, {3}, {4}, {2}]
                // indices [0, 2, 3, 1]
                // sortedRow = [undefined, undefined, undefined, undefined]
                // sortedRow = [{1},       undefined, undefined, undefined]
                // sortedRow = [{1},       {2},       undefined, undefined]
                sortedRow[colIndex] = row[indices[colIndex]];
            }
            this._data[rowIndex] = sortedRow;
        }
    }

    /*
    
    -------------
    [h1] [h2] [h3] [h4]
    x    x    x    x
    const t = new CoolTable();
    t.addRow([{title: "Lel"}, title: "2"}, title: "3"}, title: "4"}]);
    for(i..10) {
        t.addRow({d,a,t,a});
    }

    -------------
    [  ] [h1] [h2] [h3]
    [h4] x    x    x
    [h5] x    x    x
    [h6] x    x    x

    const t = new CoolTable();
    // This would be the case, but it's watchover so we don't care
    t.setTopHeaders();
    t.setLeftHeaders();
    
    t.addRow([{title: ""}, title: "2"}, title: "3"}, title: "4"}]);
    for(i..10) {
        t.addRow({header,a,t,a});
    }

    */

    /**
     * Helper for averaging col/row data for computedValue.
     * @param {Array} colOrRow computedValue's col or row property.
     * @returns {number} Average value.
     */
    static avgData(colOrRow) {
        colOrRow = colOrRow.filter(cell => cell.value !== undefined && cell.value !== null && cell.value !== "");
        if (colOrRow.length === 0) {
            return 0;
        }
        return CoolTable.sumData(colOrRow) / colOrRow.length;
    }

    /**
     * Helper for summing col/row data for computedValue.
     * @param {Array} colOrRow computedValue's col or row property.
     * @returns {number} Summed value.
     */
    static sumData(colOrRow) {
        colOrRow = colOrRow.filter(cell => cell.value !== undefined && cell.value !== null && cell.value !== "");
        return colOrRow.reduce((a, b) => {
            if (typeof a !== "number") {
                a = a.value;
            }
            if (typeof b !== "number") {
                b = b.value;
            }
            return a + b;
        }, 0);
    }

    /**
     * Helper for retrieving the max of col/row data for computedValue.
     * @param {Array} colOrRow computedValue's col or row property.
     * @returns {number} Maximum value.
     */
    static maxData(colOrRow) {
        colOrRow = colOrRow.filter(cell => cell.value !== undefined && cell.value !== null);
        return Math.max(...colOrRow.map(v => v.value));
    }

    /**
     * Helper for retrieving the min of col/row data for computedValue.
     * @param {Array} colOrRow computedValue's col or row property.
     * @returns {number} Minimum value.
     */
    static minData(colOrRow) {
        colOrRow = colOrRow.filter(cell => cell.value !== undefined && cell.value !== null);
        return Math.min(...colOrRow.map(v => v.value));
    }

    static formatAsPercentage(value) {
        // TODO: option for n decimal points
        return `${Math.round(value * 100)}%`;
    }
    
    /**
     * Inserts the table into the DOM, as a child of the specified target.
     * @param {Element} target Target element to add table to.
     */
    insert(target) {
        target.appendChild(this._table);
    }

    /**
     * Replaces the table's data (including headers).
     * @param {Array} data 2d array of new data.
     */
    setData(data) {
        this._data = data;
        this.onDataChanged();
    }

    /**
     * Adds multiple rows of data to the table (including headers).
     * @param {Array} data 2d array of new data.
     */
    addData(data) {
        for (const row of data) {
            this._data.push(row);
        }
        this.onDataChanged();
    }

    /**
     * Adds a new row to the table's data (including headers).
     * @param {Array} data 2d array of new data. 
     */
    addRow(data) {
        this._data.push(data);
        this.onDataChanged();
    }

    /**
     * Called when the table's data was changed.
     */
    onDataChanged() {
        // Update computed values
        for (let rowIndex = 0; rowIndex < this.rowCount; rowIndex++) {
            const row = this._data[rowIndex];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                const cell = row[colIndex];
                if (cell.computedValue !== undefined) {
                    const newValue = cell.computedValue({
                        cell: cell,
                        row: this.getRow(rowIndex).filter((r) => r.computedValue === undefined),
                        col: this.getCol(colIndex).filter((c) => c.computedValue === undefined),
                        rowIndex: rowIndex,
                        colIndex: colIndex
                    });
                    if (newValue !== undefined && newValue !== null) {
                        cell.value = newValue;
                    }
                }
            }
        }

        // Update cached values
        this._hasRowHeader = (() => {
            if (this.isEmpty) {
                return undefined;
            }
    
            if (this.rowCount === 1) {
                const colHeader = this._data[0][1];
                return !CoolTable.isHeader(colHeader);
            } else {
                const rowHeader = this._data[1][0];
                return CoolTable.isHeader(rowHeader);
            }
        })();

        this._hasColHeader = (() => {
            if (this.isEmpty) {
                return undefined;
            }
            
            if (this.colCount === 1) {
                const rowHeader = this._data[1][0];
                return !CoolTable.isHeader(rowHeader);
            } else {
                const colHeader = this._data[0][1];
                return CoolTable.isHeader(colHeader);
            }
        })();

        this.replaceBody();
    }

    /**
     * Replace the table's tbody.
     */
    replaceBody() {
        const tbody = this._table.querySelector("tbody");
        if (tbody) {
            this._table.removeChild(tbody);
        }
        this._table.appendChild(this._generateBody());
    }
};
