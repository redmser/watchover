class Console {
    // Text colors
    static get FG_BLACK() { return "\x1b[30m"; }
    static get FG_RED() { return "\x1b[31m"; }
    static get FG_GREEN() { return "\x1b[32m"; }
    static get FG_YELLOW() { return "\x1b[33m"; }
    static get FG_BLUE() { return "\x1b[34m"; }
    static get FG_MAGENTA() { return "\x1b[35m"; }
    static get FG_CYAN() { return "\x1b[36m"; }
    static get FG_WHITE() { return "\x1b[37m"; }

    // Background colors
    static get BG_BLACK() { return "\x1b[40m"; }
    static get BG_RED() { return "\x1b[41m"; }
    static get BG_GREEN() { return "\x1b[42m"; }
    static get BG_YELLOW() { return "\x1b[43m"; }
    static get BG_BLUE() { return "\x1b[44m"; }
    static get BG_MAGENTA() { return "\x1b[45m"; }
    static get BG_CYAN() { return "\x1b[46m"; }
    static get BG_WHITE() { return "\x1b[47m"; }

    // Other control codes
    static get RESET() { return "\x1b[0m"; }
    static get BRIGHT() { return "\x1b[1m"; }
    static get DIM() { return "\x1b[2m"; }
    static get UNDERSCORE() { return "\x1b[4m"; }
}

/**
 * Minimal logger class.
 */
module.exports.Logger = class Logger {
    /**
     * Create a new logger instance.
     * @param {number} level Current logger level.
     */
    constructor(level) {
        this.level = level || 0;
    }

    /**
     * Logs to console.
     * @param {object} level A level object, as defined in the levels array.
     * @param  {...any} msg Objects to be logged.
     * Strings support special features: `*bold*` `_underline_`
     */
    log(level, ...msg) {
        // Check if logger's configured level matches the logged message's level
        if (level.level < this.level) {
            return;
        }

        const now = new Date().toLocaleString(undefined, { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit", second: "2-digit" });
        const displayLevel = `[${level.name}]`.padEnd(longest + 3); // 3 is the length of "[] "
        const formatted = msg.map((value) => {
            if (typeof value === "string") {
                value = value.replace(/\*(.*?)\*/g, `${Console.BRIGHT}$1${Console.RESET}${level.colors || ""}`);
                value = value.replace(/_(.*?)_/g, `${Console.UNDERSCORE}$1${Console.RESET}${level.colors || ""}`);
                value = value.replace(/(\r?\n)/g, `$1${" ".repeat(displayLevel.length + now.length + 2)}`); // 2 is the length of ": "
            }
            return value;
        });
        const prefix = `${level.colors || ""}${displayLevel}${now}:`;
        console.log(prefix, ...formatted, Console.RESET);
    }

    /**
     * Gets the logger's current level.
     * @returns {number} Logger level.
     */
    get level() {
        return this._level;
    }

    /**
     * Sets the logger's current level.
     * @param {number} value Logger level.
     */
    set level(value) {
        this._level = value;
    }
};

/**
 * Current logger instance.
 */
module.exports.current = new this.Logger();

/**
 * List of log levels.
 */
module.exports.levels = [
/* eslint-disable no-multi-spaces */
    {name: "verbose",  level: 0, colors: Console.FG_GREEN},
    {name: "verb",     level: 0},
    {name: "debug",    level: 1, colors: Console.FG_CYAN},
    {name: "info",     level: 2, colors: Console.FG_WHITE},
    {name: "log",      level: 2}, // Synonym
    {name: "warning",  level: 3, colors: Console.FG_YELLOW},
    {name: "warn",     level: 3}, // Synonym
    {name: "error",    level: 4, colors: Console.FG_RED},
    {name: "err",      level: 4}, // Synonym
    {name: "critical", level: 5, colors: Console.FG_WHITE + Console.BG_RED},
    {name: "crit",     level: 5}, // Synonym
/* eslint-enable no-multi-spaces */
];

// Load levels
let longest = 0;
for (const level of this.levels) {
    if (level.name.length > longest) {
        longest = level.name.length;
    }

    module.exports[level.name] = function (...msg) {
        // Find the first logger level with the same value, to skip synonyms.
        const original = this.levels.filter((other) => level.level === other.level)[0];
        this.current.log(original, ...msg);
    };
}
